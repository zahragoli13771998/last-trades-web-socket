const {
    Op
} = require('sequelize');
const {
    Sequelize,
    DataTypes,
    Model
} = require('sequelize');
const sequelize = new Sequelize('')
class Trade extends Model {}
Trade.init({
    currencyId1: {
        type: DataTypes.INTEGER

    },
    currencyId2: {
        type: DataTypes.INTEGER

    },
    pairId: {
        type: DataTypes.INTEGER

    },
    price: {
        type: DataTypes.STRING
    },
    amount1: {
        type: DataTypes.STRING
    },
    amount2: {
        type: DataTypes.STRING
    },
    type: {
        type: DataTypes.STRING
    },
    transactionIdMd5: {
        type: DataTypes.STRING
    },
    unixTime: {
        type: DataTypes.INTEGER
    }
}, {
    sequelize
});


async function create(transactions) {
    const c = await Trade.create({
        currencyId1: transactions.currencyId1,
        currencyId2: transactions.currencyId2,
        pairId: transactions.pairId,
        price: Math.floor(transactions.price),
        amount1: transactions.amount1,
        amount2: transactions.amount2,
        type: transactions.type,
        transactionIdMd5: transactions.transactionIdMd5,
        unixTime: transactions.unixTime
    });
}

function getLastTrades(PairId) {

    return Trade.findAll({
        attributes: ['price','amount2','unixTime','type','transactionIdMd5','pairId'],
        where: {
            pairId: {
                [Op.eq]: PairId,
            },
        },
        order: [
            ['unixTime', 'DESC']
        ],
        limit: 20

    });
}


function calculatVolume(PairId, param, now, yesterday) {
    return Trade.sum(param, {
        where: {
            pairId: {
                [Op.eq]: PairId
            },
            unixTime: {
                [Op.lt]: now,
                [Op.gte]: yesterday
            }
        }
    })
}


async function deletetrades() {
    const yesterday = Math.floor(new Date().getTime() / 1000) - 86400;

    await Trade.destroy({
        where:{
            unixTime:{
                [Op.lt]:yesterday
            }
        }
        // truncate: true
    });
}

async function findOHLC(PairId, filter, order, now, yesterday) {

    return await Trade.findAll({
        attributes: ['price'],
        where: {
            pairId: {
                [Op.eq]: PairId,
            },
            // unixTime: {
            //     // [Op.lt]: now,

            //     [Op.gt]: yesterday
            // }
        },
        limit: 1,
        order: [
            [filter, order]
        ]
    });
}



module.exports = {
    create,
    findOHLC,
    getLastTrades,
    calculatVolume,
    deletetrades
}