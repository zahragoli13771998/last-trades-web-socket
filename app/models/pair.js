const { Sequelize, DataTypes, Model } = require('sequelize');
const sequelize = new Sequelize('')
class PairDetail extends Model { }
PairDetail.init({
    baseEnName: {
        type: DataTypes.STRING
    },
    baseFaNname: {
        type: DataTypes.STRING

    },
    quoteEnNname: {
        type: DataTypes.STRING

    },
    quoteFaNname: {
        type: DataTypes.STRING

    },
    baseCurrencyId: {
        type: DataTypes.INTEGER
    },
    quoteCurrencyId: {
        type: DataTypes.INTEGER
    },

    PairId: {
        type: DataTypes.INTEGER
    },

    basePersison: {
        type: DataTypes.INTEGER
    },
    quotePersison: {
        type: DataTypes.INTEGER
    },
}
    , { sequelize });

async function create(info) {

    const c = await PairDetail.create({

        baseEnName: info.baseEnName,
        baseFaNname: info.baseFaNname,
        quoteEnNname: info.quoteEnNname,
        quoteFaNname: info.quoteFaNname,
        baseCurrencyId: info.baseCurrencyId,
        quoteCurrencyId: info.quoteCurrencyId,
        PairId: info.PairId,
        basePersison: info.basePersison,
        quotePersison: info.quotePersison

    });

}

async function getDetail(pair_id) {
    const x = PairDetail.findOne({
        where: {
            PairId: pair_id
        }
    });

    return x;

}
async function getParam() {
    return PairDetail.findAll();
}

function deletePairDetail() {
    PairDetail.destroy({
        truncate: true
    });
}

module.exports = { create, getDetail, getParam, deletePairDetail };