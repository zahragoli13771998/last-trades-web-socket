const pairManager = require('../Logic/PairManager');
const pairModel = require('../models/pair')
const schedule = require('node-schedule');
async function runSchedule() {
  
    const job = schedule.scheduleJob('30 23 */2 * *', async function () {

        await pairModel.deletePairDetail();

        await pairManager.savePairs();
      
    }
    );
}
runSchedule();

module.exports ={runSchedule}