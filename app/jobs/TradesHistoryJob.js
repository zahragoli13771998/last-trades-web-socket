const schedule = require('node-schedule');
const TradeManager= require('../Logic/TradesManager');

function runSchedule() {

    const job = schedule.scheduleJob('*/1 * * * *', async function () {
        await TradeManager.setTradesHistory();

    });

}
runSchedule();
module.exports ={runSchedule}