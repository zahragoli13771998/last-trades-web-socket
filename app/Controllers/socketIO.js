const app = require('express')();
const http = require('http').Server(app);


const redis = require('redis');
const tradesModel = require("../models/trade");
const manager = require('../Logic/TradesManager');
const pairmanager = require('../Logic/PairManager');

const port = process.env.PORT || 1212;


async function getTradesHistory() {

    const client = redis.createClient();
    await client.connect();
    let markets = await pairmanager.getPairs();

    // console.log(markets)
    for (var market of markets) {
        const transactions = await tradesModel.getLastTrades((Number)(market));
        let list = [];

        for (var transaction of transactions) {

            const unix = (Number)(transaction.unixTime)
            var editedUnix = new Date(unix * 1e3).toISOString()
            var unixToTimeStamp = editedUnix.toString().replace('T', " ");
            var timeStamp = unixToTimeStamp.toString().split(".");
            const md5 = (transaction.transactionIdMd5.toString().substr(0, 32));

            const data = [(Number)(transaction.price), transaction.amount2, timeStamp[0], transaction.type, transaction.unixTime, md5, transaction.pairId]

            list.push(data)
        }
        await client.set('trades_history_of:' + market, JSON.stringify(list));

    }
}
app.get('/last_trades/*', async function (req, res) {

    const pairId = req.query.pair_id;
    await client.connect();
    const list = await client.get('trades_history_of:' + pairId);


    // console.log(data)
    // }


    res.json(list);
});

getTradesHistory();
app.listen(1215);
module.exports = {getTradesHistory}