const app = require('express')();
const Sentry = require('@sentry/node');
const Tracing = require("@sentry/tracing");
const http = require('http').Server(app);
var server = app.listen(1212);


const io = require('socket.io')().listen(server);

const port = process.env.PORT || 1212;


app.get('/trades/*', (req, res) => {

    res.sendFile(__dirname + '/test.html');
});


app.get('/trades_history', async function (req, res) {
    const pairId = req.query.pair_id;

    const x = await inputStruct.getTradesHistory((Number)(pairId));
    res.json(x);

});


const redis = require('redis');
const inputStruct = require("../Logic/TradesManager");
const tradesModel = require("../models/trade");


const {
    Kafka,
    logLevel
} = require("kafkajs");
const kafka = new Kafka({
    logLevel: logLevel.INFO,
    brokers: [``],
    clientId: 'example-consumer',


})


const TradeModel = require('../models/trade');
const PairManager = require('../Logic/PairManager');

async function orgnizeTransactions(pairID) {

    const nsp = io.of(`/${pairID}`);
    nsp.on('connection', (socket) => {
        console.log(socket.rooms)
        socket.on('', msg => {
            console.log(msg);
        });

    });


    const topic = ''
    const consumer = kafka.consumer({
        groupId: '' + pairID.toString() + '-'
    })

    await consumer.connect()
    await consumer.subscribe({
        topic,
        fromBeginning: true
    })

    await consumer.run({

        eachMessage: async ({
            topic,
            partition,
            message
        }) => {

            const transaction = await inputStruct.generateData(message.value.toString(), message.key.toString(), pairID);

            if (transaction !== 1) {
                const unix = (Number)(transaction.unixTime)
                var editedUnix = new Date(unix * 1e3).toISOString()
                var unixToTimeStamp = editedUnix.toString().replace('T', " ");
                var timeStamp = unixToTimeStamp.toString().split(".");
                nsp.volatile.emit('', `[${[transaction.price, transaction.amount2,timeStamp[0], transaction.type,transaction.unixTime, transaction.transactionIdMd5]}]`);
            }

        }
    })
    consumer.commitOffsets([
        {topic: '',partition:0, offset: '9277617'}
    ])


}
app.use(
    Sentry.Handlers.errorHandler({
        shouldHandleError(error) {
            // Capture all 404 and 500 errors
            if (error.status === 404 || error.status === 500 || error.status===502) {
                return true;
            }
            return false;
        },
    })
);

async function generateLastTradesListPerMarket() {

    let markets = await PairManager.getPairs();
    for (var market of markets) {
    //     console.log(market)

        orgnizeTransactions(market.toString());
    }
    // await new Promise(r => setTimeout(r, 20));

}


generateLastTradesListPerMarket();