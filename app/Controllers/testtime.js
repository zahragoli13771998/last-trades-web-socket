const app = require('express')();
const http = require('http').Server(app);


const redis = require('redis');
const tradesModel = require("../models/trade");
const manager = require('../Logic/TradesManager');
const pairmanager = require('../Logic/PairManager');

const port = process.env.PORT || 1212;
const client = redis.createClient();

app.get('/last_trades/*', async function (req, res) {

    const pairId = req.query.pair_id;
   
    const list = await  manager.getTradesHistory((Number)(pairId))


    // console.log(data)
    // }


    res.json(list);
});


app.listen(1215);
