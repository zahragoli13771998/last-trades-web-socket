const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const ohlcManager = require('../Logic/financialManager');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(express.static("public"));

app.get('/financial/', async function (req, res) {
    const pairId = req.query.pair_id;

    const x = await ohlcManager.getFinancial((Number)(pairId));
    res.json(x);
})
app.listen(1215, function () {
    console.log("server started on port 1215")
});