const {
    md5
} = require("request/lib/helpers");
const redis = require('redis');
const pairManager = require('../Logic/PairManager');
const tradesModel = require('../models/trade');

async function generateData(value, key, pairID) {
    const id = pairID.toString()
    const title = key.toString()
    const now = Math.floor(new Date().getTime() / 1000);
    const twodaysago = Math.floor(new Date().getTime() / 1000) - 172800;

    const precision = await pairManager.getPersision(pairID);
    if (title.includes('buy_sell_')) {
        const tradeTime = (Number)(title.substr(1, 10));
        if (tradeTime < now && tradeTime > twodaysago) {
            if (id === JSON.parse(value).pair_id.toString()) {
                const transactionId = title.replace('_buy_sell_', '').toString();
                const transactionIdMd5 = md5(transactionId)
                const price = (JSON.parse(value).amount1 * (Math.pow(10, -precision.quotePersison))) / ((JSON.parse(value).amount2 * (Math.pow(10, -precision.basePersison))));
                const amout2withPersision = JSON.parse(value).amount2 * (Math.pow(10, -precision.basePersison - 2));
                const data = {
                    currencyId1: JSON.parse(value).currency_id1,
                    currencyId2: JSON.parse(value).currency_id2,
                    price: price.toFixed(2),
                    pairId: JSON.parse(value).pair_id,
                    amount1: JSON.parse(value).amount1,
                    amount2: amout2withPersision.toFixed(2),
                    type: JSON.parse(value).type,
                    unixTime: JSON.parse(value).created_at,
                    transactionIdMd5: transactionIdMd5,
                }

                tradesModel.create(data);

                return data;
            }
            return 1;
        }

        return 1;
    }
    return 1;
}


module.exports = {
    generateData,


}


async function getlastTrades(id) {

    let v = [];
    const client = redis.createClient();
    await client.connect();
    const relatedKeys = await client.keys('*trades-pair-for:' + id + 'transaction-number*');
    for (var l of relatedKeys) {
        console.log(await client.get(l));
        v.push(await client.get(l));
    }
    return v;
    // console.log(v);
}


module.exports = {
    generateData,
    getlastTrades,
}