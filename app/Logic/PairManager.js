const fetch = require("node-fetch");
const pairModel = require('../models/pair');
function getData() {
    let lists = [];
    return fetch('').then(res => res.json())
        .then(data => lists = data['data']);
}


async function savePairs() {
    var data = await getData();
    data.forEach(function (obj) {
        const newdata = {
        
            baseEnName: obj.name.en,
            baseFaNname: obj.name.fa,
            quoteEnNname: obj.quote_currency_symbol.en,
            quoteFaNname: obj.quote_currency_symbol.fa,
            baseCurrencyId: obj.base_currency_id,
            quoteCurrencyId: obj.quote_currency_id,
            PairId: obj.pair_id,
            basePersison: obj.base_precision,
            quotePersison: obj.quote_precision
        }
        pairModel.create(newdata);

    })

}

async function getPersision(PairId) {
    const pair = await pairModel.getDetail(PairId);
    const persision = {
        basePersison: pair.basePersison,
        quotePersison: pair.quotePersison
    }
    return persision;
}


async function getPairs() {
    let pairsList = [];

    const Pairs = await pairModel.getParam();
    Pairs.forEach(function (pair) {
        pairsList.push(pair.PairId);
    });
    return pairsList;
}

module.exports = { savePairs, getPairs,getPersision } = { savePairs, getPairs,getPersision }